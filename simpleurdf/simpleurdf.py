import os

from lxml import etree  # nosec

from simpleurdf.urdf2model import Model
from simpleurdf.urdf_parser.urdf2_to_urdf import Urdf2ToUrdf


class SimpleURDF:
    def create_urdf_string(self, robot: Model) -> str:
        model = robot.build()
        tree_robot = Urdf2ToUrdf().create_robot(model)
        return etree.tostring(tree_robot, pretty_print=True, encoding=str)
        # world = urdfSerializer.createWorld(world)
        # etree.indent(world)
        # etree.ElementTree(world).write(open(pathToFile, "wb"))

    def create_urdf_file(self, robot: Model, path_to_file: str):
        urdf_robot = self.create_urdf_string(robot)
        path_to_directory, _ = os.path.split(path_to_file)
        os.makedirs(path_to_directory, exist_ok=True)
        with open(path_to_file, "w+", encoding="utf-8") as file:
            file.write(urdf_robot)
