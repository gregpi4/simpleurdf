from simpleurdf.urdf2model.basemodel import Limit, PrismaticJointType, NO_EFFORT_LIMIT
from simpleurdf.urdf2model.metamodel import DynamicsModel
from simpleurdf.urdf2model import (
    Box,
    Cylinder,
    FixedJointType,
    Joint,
    MaterialColor,
)
from simpleurdf.urdf2model import UrdfFactory
from simpleurdf.urdf2model import Model


class IndustrialPress(UrdfFactory):
    def build_model(self, xUpperLimit, xDownLimit) -> Model:
        return Model(
            name="industrial_press",
            root_link=Box(
                name="base_link",
                material=MaterialColor(name="red", ambient=[1, 0, 0]),
                width=3,
                depth=2,
                height=2,
                mass=10,
                joints=[
                    Joint(
                        name="press_joint",
                        joint_type_characteristics=PrismaticJointType(
                            translation_axis=[0, 0, 1],
                            limit=Limit(
                                lower=xDownLimit,
                                upper=xUpperLimit,
                                effort=NO_EFFORT_LIMIT,
                                velocity=0.5,
                            ),
                        ),
                        child=Cylinder(
                            name="press_link",
                            radius=0.5,
                            length=5,
                            mass=10,
                        ),
                    )
                ],
            ),
        )
