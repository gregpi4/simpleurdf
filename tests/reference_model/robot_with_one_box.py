from simpleurdf.urdf2model.basemodel import Box, MaterialColor, Model, Pose
from simpleurdf.urdf2model.model_factory import UrdfFactory


class RobotWithOneBox(UrdfFactory):
    def build_model(self) -> Model:
        width = 0.1
        height = 2.0
        mass = 1
        orange = MaterialColor("orange")
        return Model(
            name="test_robot",
            root_link=Box(
                name="baseLink",
                material=orange,
                width=width,
                depth=width,
                height=height,
                mass=mass,
                pose=Pose(
                    xyz=[
                        0,
                        0,
                        height / 2,
                    ],
                ),
            ),
        )
