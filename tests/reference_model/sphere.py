from simpleurdf.urdf2model.basemodel import Box, MaterialColor, Model, Pose, Sphere
from simpleurdf.urdf2model.model_factory import UrdfFactory


class SphereRobot(UrdfFactory):
    def build_model(self) -> Model:
        mass = 1
        green = MaterialColor("green")
        return Model(
            name="sphere",
            root_link=Sphere(name="baseSphere", material=green, radius=1, mass=mass),
        )
